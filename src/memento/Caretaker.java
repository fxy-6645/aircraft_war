package memento;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.rjxy.fxy.controler.DB_Operater;

public class Caretaker {
	//database
	private DB_Operater db_Operater= new DB_Operater();
	private Memento mem= new Memento();

	public Memento getMemento(){
		//第一步：先去数据库里获取该战机的分数和等级
		int score = db_Operater.get("fxy", "maxgrade");
		int level = db_Operater.get("fxy", "level");
		//第二步:把获取到的score,level封装的Memento的对象mem中
		mem.setLevel(level);
		mem.setScore(score);
		//第三步:返回
		return mem;
	}
	
	public void saveMemento(Memento mem){
		db_Operater.update("fxy", mem.getLevel(), mem.getScore());
		System.out.println("update......");
	}

}
