package com.rjxy.fxy.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import memento.Caretaker;
import memento.Memento;

import com.rjxy.fxy.bean.Hero;
import com.rjxy.fxy.controler.DB_Operater;

public class Start extends JFrame implements ActionListener {
	private static final long serialVersionUID = 2;
	private JButton button_start = new JButton();
	private JButton button_end = new JButton();
	
	private static int level;
	private static int grade;
	private static int time;
	private static String name;
	
	
	/*public static void main(String[] args) {
		new Start(grade, level, name, time);
	}*/
	
	//public Start() {

	public Start(int level, int grade, String name, int time) {
		this.level = level;
		this.grade = grade;
		this.name = name;
		this.time = time;
		DB_Operater db_Operater = new DB_Operater();
		
		//Image img = Toolkit.getDefaultToolkit().createImage();//新增背景
		JLabel ctbg;
		ctbg = new JLabel(new ImageIcon("img/ctbg.jpg") );
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		ctbg.setBounds((dim.width - 300) / 2, (dim.height - 400) / 2, 400, 500);
		JPanel imagePanel = (JPanel)this.getContentPane();
		ctbg.setOpaque(false);
		this.getLayeredPane().add(ctbg,new Integer(Integer.MIN_VALUE));
		
		JFrame frame = new JFrame("开始界面");
		frame.setBounds((dim.width - 300) / 2, (dim.height - 400) / 2, 400, 500);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		button_start.setText("开始游戏");
		button_start.addActionListener(this);
		button_end.setText("结束游戏");
		button_end.addActionListener(this);
				
		//JPanel panel = new JPanel();
		JPanel panel1 = new JPanel();
		
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		JPanel panel5 = new JPanel();
		
		JTextField Level = new JTextField(20);  
		JTextField Grade = new JTextField(20);
		JTextField Time = new JTextField(20);
		
		Level.setEnabled(false);
		Grade.setEnabled(false);
		Time.setEnabled(false);
		
		Level.setText(level+"");
		Grade.setText(grade+"");
		Time.setText(time+"秒");
		
		
		panel1.add(button_start);
		panel1.add(button_end);
		
		panel2.add(new Label("等        级："));  
        panel2.add(Level);  
        panel3.add(new Label("成        绩："));  
        panel3.add(Grade);
        panel4.add(new Label("累计在线："));
        panel4.add(Time);        

        		
        panel5.add(panel2,BorderLayout.NORTH);
        panel5.add(panel3,BorderLayout.CENTER);
        panel5.add(panel4,BorderLayout.SOUTH);

        
		frame.add(panel1, BorderLayout.SOUTH);
		
		frame.add(panel5, BorderLayout.CENTER);
		
		//panel.add(ctbg);
		//frame.add(panel,BorderLayout.CENTER);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button_start) {
			//可以加上恢复备忘录
			new Started(level, grade, name);
		} 
		if (e.getSource() == button_end) {
			//添加备忘录
			Caretaker ct = new Caretaker();
			ct.saveMemento(Hero.getHero().createMemento());//调原发者里的mem
			ct.getMemento();
			System.exit(0);   //  点击结束游戏退出
			
			
					
		}
	}
}






