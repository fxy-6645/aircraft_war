package com.rjxy.fxy.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.rjxy.fxy.controler.DB_Operater;

/*注册界面，实现注册和登录窗口
 * 
 */
public class Jm_start extends JFrame implements MouseListener{
	//按钮
	private JButton bt_signup;
	private JButton bt_login;
	private JTextField tf_name;
	private JTextField tf_pass;
	
	DB_Operater db_Operater;
	
	Jm_start(){
		db_Operater = new DB_Operater();
		
		this.setTitle("飞机大战");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		//文本框
		JPanel p_text1 = new JPanel();
		JPanel p_text2 = new JPanel();
		tf_name = new JTextField(20);//用户名和密码后面的文本框
		tf_pass = new JTextField(20);
		p_text1.add(new Label("用户名"));
		p_text2.add(new Label("密    码"));
		p_text1.add(tf_name);
		p_text2.add(tf_pass);
		
		//按钮栏
		JPanel p_bt = new JPanel();
		bt_signup = new JButton("注册账号");
		bt_login = new JButton("登录游戏");
		p_bt.add(bt_signup);
		p_bt.add(bt_login);
		
		//监听器
		bt_signup.addMouseListener(this);
		bt_login.addMouseListener(this);
		
		this.add(p_text1, BorderLayout.NORTH);
		this.add(p_text2, BorderLayout.CENTER);
		this.add(p_bt, BorderLayout.SOUTH);
		this.setBounds((dim.width - 400) / 2, (dim.height - 300) / 2, 350, 150);// 界面居中
		this.setResizable(false);// 大小不可改变
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public static void main(String []args){
		new Jm_start();
	}

	public void mouseClicked(MouseEvent e) {
		//获取文本框，去掉首尾空格
		String name = tf_name.getText().toString().trim();
		String pass = tf_pass.getText().toString().trim();
		
		if(e.getSource() == bt_signup){
		if(name.equals("")||name.length()==0||pass.equals("")||pass.length()==0){
			//输出错误信息消息提示框
			JOptionPane.showMessageDialog(null, "请输入账号和密码！");
			}else{
				if(db_Operater.exist(name)){
					JOptionPane.showMessageDialog(null, "该账号已存在，请重新输入！");
				}else{
					db_Operater.add(name, pass);
					JOptionPane.showMessageDialog(null, "注册成功，请登录！");
				}
			}
		} else if (e.getSource() == bt_login) { 
			if (name.equals("")||name.length()==0||pass.equals("")||pass.length()==0) {
				// 输出错误信息
				JOptionPane.showMessageDialog(null, "请输入账号和密码!");
			} else {
				if( db_Operater.check(name, pass) ) {  //登陆成功
					// 跳转主界面
					this.setVisible(false);
					int level = db_Operater.get(name, "level");
					int grade = db_Operater.get(name, "grade");
					int time = db_Operater.get(name, "time");
					//new Start();
					new Start(level, grade, name, time);
				} else {
					// 不跳转 输出错误信息
					JOptionPane.showMessageDialog(null, "请正确的账号和密码!");
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
