package com.rjxy.fxy.util;

import com.rjxy.fxy.bean.Bullet;
import com.rjxy.fxy.bean.EnemyPlane;
import com.rjxy.fxy.bean.Hero;

public class PlaneTools {

	//判断敌机是否与子弹相撞
		public static boolean isHeat(Bullet bu, EnemyPlane ep) {
			if ((bu.getX() - ep.getX()) * (bu.getX() - ep.getX()) + 
					(bu.getY() - ep.getY()) * (bu.getY() - ep.getY()) <=
					(bu.getR() + ep.getR()) * (bu.getR() + ep.getR())) {
				return true;
			} else {
				return false;
			}
		}

		//判断敌机是否与战机相撞
		public static boolean isHeat(EnemyPlane ep, Hero hero) {
			if ((ep.getX() - hero.getX()) * (ep.getX() - hero.getX()) + 
					(ep.getY() - hero.getY()) * (ep.getY() - hero.getY()) <=
					(ep.getR() + hero.getR()) * (ep.getR() + hero.getR())) {
				return true;
			} else {
				return false;
			}
		}
}
