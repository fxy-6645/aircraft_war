package com.rjxy.fxy.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCTools {
	public static Connection getConnection() throws Exception{
		//创建properties类读取配置文件
		Properties properties = new Properties();
		//加载资源 传入输入流
		InputStream inStream = JDBCTools.class.getClassLoader().getResourceAsStream("jdbc.properties");
		properties.load(inStream);
		//1准备获取连接的4个字符串：user，password，url，jdbcDriver
		String user = properties.getProperty("user");
		String password = properties.getProperty("password");
		String url = properties.getProperty("url");
		String jdbcDriver = properties.getProperty("jdbcDriver");
		//2.加载驱动：Class.forName(driverClass)
		Class.forName(jdbcDriver);
		//3、获取数据库连接
		Connection connection = DriverManager.getConnection(url,user,password);
		return connection;
		
	}
	public static void releaseDB(ResultSet resultSet,Statement statement,Connection connection){
		if(resultSet !=null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(statement !=null){
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(connection != null){
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
