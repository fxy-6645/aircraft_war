package com.rjxy.fxy.bean;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import memento.Memento;

import com.rjxy.fxy.state.Hero1stState;
import com.rjxy.fxy.state.HeroState;

/**
 * 战机类
 * @author teacher
 *
 */
public class Hero {
	private int x;
	private int y;
	private int r;
	private int AllBlood;
	private int blood;
	private int score;
	private Image heroImg = new ImageIcon("img/hero.png").getImage();
	
	private int level = 1;//4
	private HeroState currenState = new Hero1stState(this);//4
	
	//以下为1.12日第5次实训增加内容：单例模式1：饿汉式
	/*public static Hero hero = new Hero();
	private Hero(){}
	public static Hero getHero(){//不加static则无法调用该方法，因为无法new对象，加上则为类方法，可通过类名打点调用
		return hero;
	}*/
	
	//单例模式2：懒汉式
	private static Hero hero;
	private Hero(){}
	public static synchronized Hero getHero(){
		if(hero == null){
			hero = new Hero();
		}
		return hero;
	}
	
	//以下是备忘录模式1月12日,hero即为原发者
	public Memento createMemento(){
		Memento mem = new Memento();
		//mem.setScore(this.score);  这样也可以
		mem.setScore(hero.getScore());
		mem.setLevel(hero.getLevel());//体现封装性
		return mem;
	}
	public void restoreMemento(Memento mem){
		//从备忘录恢复，即获取战机上一次的分数等级
		level = mem.getLevel();
		score = mem.getScore();
	}
	
	public  boolean isLive(){
		if(hero.getBlood()>0){
			return true;
		}else
			return false;
	}
	
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public HeroState getCurrenState() {
		return currenState;
	}
	public void setCurrenState(HeroState currenState) {
		this.currenState = currenState;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
		currenState.check();//调用状态检测方法：Hero1stState
	}
	public int getAllBlood() {
		return AllBlood;
	}
	public void setAllBlood(int allBlood) {
		AllBlood = allBlood;
	}
	public int getBlood() {
		return blood;
	}
	public void setBlood(int blood) {
		this.blood = blood;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	
	public void paint(Graphics g) {
		/*g.setColor(Color.YELLOW);
		g.fillOval(x - r, y - r, r * 2, r * 2);*/
		g.drawImage(heroImg, x - r, y - r, null);
		//绘制总血条
		g.setColor(Color.ORANGE);
		g.fillRect(20, 20, AllBlood, 30);
		//绘制剩余血条
		g.setColor(Color.RED);
		g.fillRect(20, 20, blood, 30);
		g.setFont(new Font("宋体", Font.BOLD, 20));
		g.drawString("分数：" + score, 900, 50);
		
		g.drawString("Level：" + level, 1200, 50);//4
	}
	
	/*public HeroState correntState(){
		
	}*/
}












