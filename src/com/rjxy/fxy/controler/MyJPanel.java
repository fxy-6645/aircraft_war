package com.rjxy.fxy.controler;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.rjxy.fxy.bean.Bullet;
import com.rjxy.fxy.bean.EnemyPlane;
import com.rjxy.fxy.bean.Hero;
import com.rjxy.fxy.state.Hero3rdState;
import com.rjxy.fxy.util.PlaneTools;

public class MyJPanel extends JPanel implements Runnable,MouseMotionListener {
	private static final long serialVersionUID = 5179918663894372869L;
	private Hero hero = Hero.getHero();//类名打点调方法
	private int bgX;
	private int bgY;
	
	private int by;//4.第一幅背景图像纵坐标，两幅完全一样
	private int bby;//4.第二幅背景图像，两幅图像隔几秒显示一次，从上往下运动，改变纵坐标
	

	//敌机集合
	private ArrayList<EnemyPlane> enemyPlanes = new ArrayList<EnemyPlane>();
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private int epNum = 10;//敌机数量
	private Image bgImg = null;
	private Image goImg = new ImageIcon("img/GameOver.jpg").getImage();
	
	// database 
	private DB_Operater db_Operater;
	private String name;
	private int curTime, lastTime, grade;
	

	public MyJPanel(Dimension dim, int grade, String name) {
		
		lastTime = (int)System.currentTimeMillis();
		// database
		this.name = name;
		this.grade = grade;
		db_Operater = new DB_Operater();
		
		//初始化屏幕的宽和高
		bgX = dim.width;
		bgY = dim.height;
		
		
		//4对背景初始化
		by=0;
		bby = by -bgY;

		//初始化战机
		hero.setR(57);
		hero.setX((bgX - hero.getR() * 2) / 2);
		hero.setY(bgY - 3 * hero.getR());
		hero.setAllBlood(150);
		hero.setBlood(150);
		hero.setScore(0);
		
		//初始化敌机
		for (int i = 0; i < epNum; i++) {
			EnemyPlane ep = new EnemyPlane();
			ep.setX((int) (Math.random() * bgX));
			ep.setY((int) (Math.random() * bgY));
			ep.setR(25);
			ep.setSpeed((int) (Math.random() * 5) + 1);
			enemyPlanes.add(ep);
		}
		
		//初始化背景
		int bgNum = (int) (Math.random() * 5) + 1;
		bgImg = new ImageIcon("img/bg2.jpg").getImage();
		bgImg = new ImageIcon("img/bg" + bgNum + ".jpg").getImage();
		
		//设置鼠标监听
		this.addMouseMotionListener(this);
	}
	
	//切换背景,由于背景只有5幅图片，当等级大于5时，就要模除5取余
	public void inintBg(){
		int bgNum = hero.getLevel()%5 + 1;
		bgImg = new ImageIcon("img/bg"+bgNum + ".jpg").getImage();
	}
	
	//背景的移动
	public void move(){
		by += 2;
		bby += 2;
		if(by > bgY){
			by = bby - bgY;
		}
		if(bby > bgY){
			bby = by -bgY;
		}
	}
	

	public void paint(Graphics g) {
		super.paint(g);
		//设置背景色
//		this.setBackground(Color.BLACK);
		this.inintBg();
		
		//g.drawImage(bgImg, 0, 0, bgX, bgY, null);        重新绘制背景图片
		g.drawImage(bgImg, 0, by, bgX, bgY, null);
		g.drawImage(bgImg, 0, bby, bgX, bgY, null);
		move();

		//绘制敌机
		for (int i = 0; i < enemyPlanes.size(); i++) {
			enemyPlanes.get(i).paint(g);
		}		
		//绘制子弹
		for (int i = 0; i < bullets.size(); i++) {
			Bullet bu = bullets.get(i);
			bu.paint(g);
		}
		//绘制战机
		hero.paint(g);
		
		//GAMEOVER
		if (hero.getBlood() <= 0) {
			
			JOptionPane.showMessageDialog(null,"您此次游戏的最高分为："+hero.getScore()+ "\r\n "+"您的等级为："+hero.getLevel());

			
		// update the data  
		int grade = hero.getScore();
		int level = hero.getScore()/500; 
		
		curTime = (int)System.currentTimeMillis();
		int time = (curTime - lastTime)/1000 + db_Operater.get(name, "time");
		int temp = Math.max(grade, db_Operater.get(name, "maxgrade"));
		System.out.println("test:!!!"+temp);
		db_Operater.update(name, level, grade);
		db_Operater.update(name, time+"", "time");
		db_Operater.update(name, temp+"", "maxgrade");
		System.out.println("tetttt:"+db_Operater.get(name, "maxgrade"));
		
		/*g.setFont(new Font("宋体", Font.BOLD, 50));
		g.drawString("GAME OVER", (bgX - 450) / 2, (bgY - 50) / 2);*/
		g.drawImage(goImg, 0, 0, bgX, bgY, null);
		
		g.drawImage(goImg, 0, 0, bgX, bgY, null);
		g.setFont(new Font("宋体", Font.BOLD, 20));
		
		//int maxgrade = db_Operater.get(name, "maxgrade");  // get the maxgrade form the db
		int maxgrade = hero.getScore();
		
		g.drawString("最高分：" + maxgrade, (bgX - 100) / 2, bgY - 200);
		}
	}


	@Override
	public void run() {
		int count = 0;
		while (true) {
			
			if (hero.getBlood() > 0) {
				//初始化子弹
				if (count % 10 == 0) {
					initBullet();
				}
				count++;
				
				//让敌机下落
				for (int i = 0; i < enemyPlanes.size(); i++) {
					EnemyPlane ep = enemyPlanes.get(i);
					if (ep.getY() >= bgY) {
						ep.setX((int) (Math.random() * bgX));
						ep.setY(0 - ep.getR());
						ep.setSpeed((int) (Math.random() * 5) + 1);
						ep.changeImg();
						enemyPlanes.remove(i);
						enemyPlanes.add(ep);
					} else {
						ep.setY(ep.getY() + ep.getSpeed());
					}
					//判断敌机是否与战机相撞
					boolean b = PlaneTools.isHeat(ep, hero);
					if (b == true) {
						ep.setX((int) (Math.random() * bgX));
						ep.setY(0 - ep.getR());
						ep.setSpeed((int) (Math.random() * 5) + 1);
						ep.changeImg();
						enemyPlanes.remove(i);
						enemyPlanes.add(ep);
						hero.setBlood(hero.getBlood() - hero.getAllBlood() / 5);
					}
				}
				
				//让子弹飞
				for (int i = 0; i < bullets.size(); i++) {
					Bullet bu = bullets.get(i);
					if (bu.getY() <= 0 - bu.getR()) {
						bullets.remove(i);
					} else {
						bu.setY(bu.getY() - 10);
					}
					for (int j = 0; j < enemyPlanes.size(); j++) {
						EnemyPlane ep = enemyPlanes.get(j);
						boolean b = PlaneTools.isHeat(bu,ep);//判断子弹与敌机是否相撞
						if (b == true) {
							bullets.remove(i);
							ep.setX((int) (Math.random() * bgX));
							ep.setY(0 - ep.getR());
							ep.setSpeed((int) (Math.random() * 5) + 1);
							ep.changeImg();
							enemyPlanes.remove(j);
							enemyPlanes.add(ep);
							hero.setScore(hero.getScore() + 10);
						}
					}
				}
				repaint();
			}
			
			try {
				Thread.sleep(10);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void initBullet() {
		Bullet bu = new Bullet();
		bu.setX(hero.getX());
		bu.setY(hero.getY());
		bu.setR(50);
		bullets.add(bu);
	}

	//鼠标按下并移动时调用
	@Override
	public void mouseDragged(MouseEvent e) {
		if (hero.getBlood() > 0) {
			hero.setX(e.getX());
			hero.setY(e.getY());
			repaint();
		}
		if( !(hero.getBlood()>0)){
			hero.setX(e.getX());
			hero.setY(e.getY());
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

}







