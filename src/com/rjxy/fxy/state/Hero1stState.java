package com.rjxy.fxy.state;

import javax.swing.JOptionPane;

import com.rjxy.fxy.bean.Hero;

public class Hero1stState extends HeroState {
	public Hero1stState(Hero hero){//hero在调用该构造方法
		this.hero = hero;
	}
	public Hero1stState(HeroState state){//具体的状态在调用该构造方法
		this.hero = state.hero;
	}
	

	public void check() {
		// TODO Auto-generated method stub
		int score = hero.getScore();
		if(score > 200){
			JOptionPane.showMessageDialog(null, "Congratulations!");
			//从第二关到第一关
			hero.setCurrenState(new Hero2edState(this));
			//战机属性重新设置
			hero.setBlood(150);
			hero.setLevel(2);
		}
	
	}



}
