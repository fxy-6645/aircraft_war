package com.rjxy.fxy.state;

import javax.swing.JOptionPane;

public class Hero2edState extends HeroState {

	public Hero2edState(HeroState state) {
		hero = state.hero;
	}

	@Override
	public void check() {
		// TODO Auto-generated method stub
		int score = hero.getScore();
		if(score > 400){
			JOptionPane.showMessageDialog(null, "congratulations!");
			
			hero.setCurrenState(new Hero3rdState(this));//由地第二关到第三关
			//重设战机属性
			hero.setBlood(150);
			hero.setLevel(3);
		}

	}

}
