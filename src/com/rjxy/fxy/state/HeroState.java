package com.rjxy.fxy.state;

import com.rjxy.fxy.bean.Hero;

public abstract class HeroState {
	Hero hero;//权限为非私有的
	
	public abstract void check();//控制战机状态的切换

}
